local PointManager={nextPoint=1, points={}}
local PointFunctions={}

function PointFunctions:distanceToPt(pt)
  local dx = self.x - pt.x
  local dy = self.y - pt.y
  return math.sqrt(dx * dx + dy * dy)
end

function PointFunctions:distanceToXY(x, y)
  local dx = self.x - x
  local dy = self.y - y
  return math.sqrt(dx * dx + dy * dy)
end

function PointFunctions:angleTo(pt)
  return math.deg(math.atan2(pt.x - self.x, pt.y - self.y))
end

function PointFunctions:bouncePt()
  transition.to(self, { xScale=1.7, yScale=1.7, time=1000, transition=easing.outBounce })
  transition.to(self, { xScale=1, yScale=1, time=1000, delay=1000, transition=easing.outQuint })
end

function PointFunctions:shootTo(object, pt)
  object.x = self.x
  object.y = self.y
  object.rotation = -self:angleTo(pt)
  transition.to(object, { x=pt.x, y=pt.y, time=1000, onComplete=function (event) pt:setFillColor( unpack(pt.fillColor) ) end })
  pt:bouncePt()
  pt:setFillColor(0, 1, 0)
end

function PointManager.createPoint(x, y, radius, r, g, b)
  local c = display.newCircle(x, y, radius)
  c:setFillColor(r, g, b)
  
  for name,fn in pairs(PointFunctions) do
    c[name] = fn
  end
  c.index      = PointManager.nextPoint
  c.text       = display.newText({ text="", x=c.x, y=c.y })
  c.fillColor  = {r, g, b}
  
  getmetatable(c).__tostring = function (self)     return "(" .. self.x .. ", " .. self.y .. ")" end
  getmetatable(c).__concat   = function (lhs, rhs) return tostring(lhs) .. tostring(rhs)         end
  
  PointManager.points[PointManager.nextPoint] = c
  PointManager.nextPoint = PointManager.nextPoint + 1
  print("Create point at " .. c)
  return c
end

function PointManager.debug(state)
  for index,pt in ipairs(PointManager.points) do
    if state then 
      pt.text.text = index
    else
      pt.text.text = ""
    end
  end
end

function PointManager.free()
  for index,pt in ipairs(PointManager.points) do
    PointManager.points[index].text:removeSelf()
    PointManager.points[index]:removeSelf()
    PointManager.points[index] = nil
  end
  PointManager.nextPoint = 1
end

function PointManager.getRandomPoint()
  return PointManager.points[math.random(1, PointManager.nextPoint - 1)]
end

return PointManager
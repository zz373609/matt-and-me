local PointManager = require("PointManager")


-- Returns true if (x, y) is greater than or equal to min_distance
-- from any point in 'points'. Returns false if (x, y) is too close to
-- a point in 'points'.
function point_is_valid(x, y, points, min_distance)
  for i, pt in ipairs(points) do
    if pt:distanceToXY(x, y) < min_distance then
      return false
    end
  end
  return true
end

-- Returns a table of points in the form of:
-- points[1].x = ...
-- points[1].y = ...
-- points[2].x = ...
-- ...
-- points[num_points].x = ...
-- points[num_points].y = ...
-- Points will be at least border_widt from any edge of the screen. (measured in pixels)
function generate_points(num_points, border_width, radius, r, g, b)
  local width = display.contentWidth - border_width * 2
  local height = display.contentHeight - border_width * 2

  local min_distance = math.sqrt(width * height / (4 * num_points))
  local max_attempts = 50
  local points = {}
  for i = 1, num_points do
    local x, y
    local attempts = 0
    repeat
      attempts = attempts + 1
      x = math.random(border_width, display.contentWidth - border_width)
      y = math.random(border_width, display.contentHeight - border_width)
    until point_is_valid(x, y, points, min_distance) or attempts == max_attempts
    
    if attempts == max_attempts then
      print('generate_points: Could not place a point (try lowering min_distance)')
    else
      points[i] = PointManager.createPoint(x, y, radius, r, g, b)
    end
  end
  return points
end

bullet = display.newRect(0, 0, 20, 50)
points = generate_points(10, 50, 15, 0.8, 0.5, 0.0)

function shoot_to_next()
  local pt
  i = i or 1
  repeat
    pt = PointManager.getRandomPoint()
  until pt.index ~= i
  points[i]:shootTo(bullet, pt)
  i = pt.index
end

t = timer.performWithDelay(1000, shoot_to_next, 0)

-- This would delete all the points and DisplayObjects, and free the memory used
--PointManager.free()
-- This would stop the timer
--t.cancel()




